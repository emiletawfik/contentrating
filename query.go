package contentrating

import (
	"github.com/texttheater/golang-levenshtein/levenshtein"
	"golang.org/x/text/language"
)

// ParseRating return a rating or nil if we don't find the corresponding rating
func ParseRating(region language.Region, s string) []Rating {
	systems, ok := systemsPerRegion[region]
	if !ok {
		return nil
	}
	bestdistance := -1
	var found []Rating
	for _, sys := range systems {
		for _, r := range sys.Ratings() {
			distance := levenshtein.DistanceForStrings([]rune(s), []rune(r.Name()), levenshtein.DefaultOptions)
			if distance < bestdistance || bestdistance == -1 {
				bestdistance = distance
				found = []Rating{r}
			} else if distance == bestdistance {
				found = append(found, r)
			}
		}
	}
	if bestdistance == -1 || bestdistance > 2 {
		return nil
	}
	return found
}

var systemsPerRegion = make(map[language.Region][]System)

func prepareSystem(s *baseSystem) {
	for _, r := range s.ratings {
		r.system = s
	}
	for _, r := range s.Regions() {
		systemsPerRegion[r] = append(systemsPerRegion[r], s)
	}
}
