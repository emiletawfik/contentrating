package contentrating

import (
	"testing"

	"golang.org/x/text/language"
)

func TestParsing(t *testing.T) {
	tests := []struct {
		region    language.Region
		ratingStr string
		expected  map[string]bool
	}{
		{language.MustParseRegion("US"), "PG-13", map[string]bool{"MPAA->PG-13": true}},
		{language.MustParseRegion("FR"), "PEGI-16", map[string]bool{"PEGI->PEGI-16": true}},
		{language.MustParseRegion("BE"), "PEGI-16", map[string]bool{"PEGI->PEGI-16": true}},
		{language.MustParseRegion("FR"), "18", map[string]bool{"CSA->18": true, "CNC->18": true}},
		{language.MustParseRegion("BE"), "18", map[string]bool{"CSA->18": true}},
		{language.MustParseRegion("BE"), "DONTEXIST", nil},
	}
	for _, test := range tests {
		check := ParseRating(test.region, test.ratingStr)
		if len(test.expected) == 0 {
			if len(check) > 0 {
				t.Errorf("Expected no rating found for %v %#v", test.region, test.ratingStr)
			}
			continue
		}
		if len(check) == 0 {
			t.Errorf("Expected rating found no one for %v %#v", test.region, test.ratingStr)
			continue
		}
		if len(test.expected) != len(check) {
			t.Errorf("Expected %#v ratings got %#v for %v %#v", len(test.expected), len(check), test.region, test.ratingStr)
			continue
		}
		for _, got := range check {
			ok, _ := test.expected[got.String()]
			if !ok {
				t.Errorf("%#v shoudn't be returned for %v %#v", got.String(), test.region, test.ratingStr)
			}
		}
	}
}
