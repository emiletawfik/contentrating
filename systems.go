package contentrating

import "golang.org/x/text/language"

func init() {
	prepareSystem(INCAA)
	prepareSystem(BMUKK)
	prepareSystem(ACMA)
	prepareSystem(NCS)
	prepareSystem(CICF)
	prepareSystem(CSA_BE)
	prepareSystem(PEGI)
	prepareSystem(NFRC)
	prepareSystem(DJCTQ)
	prepareSystem(AFC)
	prepareSystem(CBSC_F)
	prepareSystem(CBSC)
	prepareSystem(CHVRS)
	prepareSystem(CPBC_T)
	prepareSystem(CPBC)
	prepareSystem(MFCB)
	prepareSystem(OFRB)
	prepareSystem(RCQ)
	prepareSystem(CH)
	prepareSystem(ANATEL)
	prepareSystem(CCC)
	prepareSystem(MOC)
	prepareSystem(CZ)
	prepareSystem(FSK)
	prepareSystem(MCCYP)
	prepareSystem(EE)
	prepareSystem(EG)
	prepareSystem(ICAA)
	prepareSystem(MEKU)
	prepareSystem(CNC)
	prepareSystem(CSA_FR)
	prepareSystem(BBFC)
	prepareSystem(GR)
	prepareSystem(FCO)
	prepareSystem(RCNOF)
	prepareSystem(LSF)
	prepareSystem(IFCOF)
	prepareSystem(IFCO)
	prepareSystem(RTE)
	prepareSystem(IL)
	prepareSystem(CBFC)
	prepareSystem(SMAIS)
	prepareSystem(AGCOM)
	prepareSystem(MBACT)
	prepareSystem(EIRIN)
	prepareSystem(KFCB)
	prepareSystem(KMRB)
	prepareSystem(CSCF)
	prepareSystem(NKC_LV)
	prepareSystem(MCCAA)
	prepareSystem(NBC)
	prepareSystem(RTC)
	prepareSystem(FCBM)
	prepareSystem(NFVCB)
	prepareSystem(Kijkwijzer)
	prepareSystem(Medietilsynet)
	prepareSystem(OFLC)
	prepareSystem(PE)
	prepareSystem(MTRCB)
	prepareSystem(NBC_PL)
	prepareSystem(CCE)
	prepareSystem(CNA)
	prepareSystem(MKRF)
	prepareSystem(SM_SA)
	prepareSystem(MDA)
	prepareSystem(SK)
	prepareSystem(BFVC)
	prepareSystem(IBMCT)
	prepareSystem(MOC_TW)
	prepareSystem(Ukraine)
	prepareSystem(ESRB)
	prepareSystem(FAB)
	prepareSystem(MPAAT)
	prepareSystem(MPAA)
	prepareSystem(RIAA)
	prepareSystem(TVPG)
	prepareSystem(RESORTE_Health)
	prepareSystem(RESORTE_Language)
	prepareSystem(RESORTE_Sexo)
	prepareSystem(RESORTE_Violencia)
	prepareSystem(MCST)
	prepareSystem(FPB)
	prepareSystem(UNRATED)
}

var ACMA = &baseSystem{
	acronym: "ACMA",
	name:    "Australian Communications and Media Authority",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("AU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "P", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
	},
}

var AFC = &baseSystem{
	acronym: "AFC",
	name:    "Alberta Film Classification",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14A", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18A", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 70},
		&baseRating{name: "R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var AGCOM = &baseSystem{
	acronym: "AGCOM",
	name:    "Autorità per le Garanzie nelle Comunicazioni",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "T", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "VM14", minRecommendAge: 14, minAllowedAgeSupervised: 14, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "VM18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var ANATEL = &baseSystem{
	acronym: "ANATEL",
	name:    "Asociación Nacional de Televisión",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvOther},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "F", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "I", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "I-7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "I-10", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "I-12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "R", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 30},
		&baseRating{name: "A", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
	},
}

var BBFC = &baseSystem{
	acronym: "BBFC",
	name:    "British Board of Film Classification",
	typ:     OrgOther,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("GB"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeGame},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail, EnvApp, EnvOther},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "U", minRecommendAge: 4, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "PG", minRecommendAge: 8, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12A", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: false, ordinal: 6},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "R18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
	},
}

var BFVC = &baseSystem{
	acronym: "BFVC",
	name:    "Board of Filmand Video Censors",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("TH"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV, TypeAd},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "13", minRecommendAge: 13, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 75},
		&baseRating{name: "20", minRecommendAge: 20, minAllowedAgeSupervised: 20, minAllowedAgeUnsupervised: 20, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "B", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 100},
	},
}

var BMUKK = &baseSystem{
	acronym: "BMUKK",
	name:    "Jugendmedienkommission beim Bundesministerium für Bildung",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("AT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "AA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "8", minRecommendAge: 8, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "10", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 18},
	},
}

var CBFC = &baseSystem{
	acronym: "CBFC",
	name:    "Central Board of Film Certification",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IN"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "U", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "UA", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "A", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "S", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 95},
	},
}

var CBSC = &baseSystem{
	acronym: "CBSC",
	name:    "Canadian Broadcast Standards Council",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvOther},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "C8", minRecommendAge: 8, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 8, hpcApplicable: true, ordinal: 4},
		&baseRating{name: "PG", minRecommendAge: 8, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "14", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 14},
		&baseRating{name: "18", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
	},
}

var CBSC_F = &baseSystem{
	acronym: "CBSC-F",
	name:    "Canadian Broadcast Standards Council",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTV},
			Environments: []RestrictionEnv{EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "8+", minRecommendAge: 8, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 8, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "13+", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16+", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "RC", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var CCC = &baseSystem{
	acronym: "CCC",
	name:    "Consejo de Calificación Cinematográfica",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvTheater, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "TE", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 70},
	},
}

var CCE = &baseSystem{
	acronym: "CCE",
	name:    "Comissão de Classificação de Espectáculos",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("PT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "M/4", minRecommendAge: 4, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "M/6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "M/12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "M/16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "M/18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var CH = &baseSystem{
	acronym: "CH",
	name:    "",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CH"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "0", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 4, minAllowedAgeUnsupervised: 6, hpcApplicable: true, ordinal: 25},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 10, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 45},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 14, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 65},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var CHVRS = &baseSystem{
	acronym: "CHVRS",
	name:    "MOTION PICTURE ASSOCIATION - CANADA",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14A", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18A", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var CICF = &baseSystem{
	acronym: "CICF",
	name:    "Commission de Sélection des Films",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("BE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "KT/EA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "KNT/ENA", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 3},
	},
}

var CNA = &baseSystem{
	acronym: "CNA",
	name:    "CONSILIUL NATIONAL AL AUDIOVIZUALULUI - CNA",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("RO"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "AP", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
	},
}

var CNC = &baseSystem{
	acronym: "CNC",
	name:    "Commission de classification cinématographique",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("FR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "T", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "interdiction", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var CPBC = &baseSystem{
	acronym: "CPBC",
	name:    "Consumer Protection BC",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14A", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18A", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 70},
		&baseRating{name: "R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 75},
		&baseRating{name: "A", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var CPBC_T = &baseSystem{
	acronym: "CPBC-T",
	name:    "Consumer Protection BC",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "AP", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "SP", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "A", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var CSA_BE = &baseSystem{
	acronym: "CSA",
	name:    "Conseil supérieur de l’audiovisuel",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("FR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "10", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "interdiction", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var CSA_FR = &baseSystem{
	acronym: "CSA",
	name:    "Conseil supérieur de l’audiovisuel",
	typ:     OrgOther,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("BE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvBroadcast, EnvRetail, EnvOther},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "10", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "interdiction", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var CSCF = &baseSystem{
	acronym: "CSCF",
	name:    "Commission de surveillance de la classification des films",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("LU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 6, minAllowedAgeUnsupervised: 6, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 16},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var CZ = &baseSystem{
	acronym: "CZ",
	name:    "",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CZ"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "U", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var DJCTQ = &baseSystem{
	acronym: "DJCTQ",
	name:    "Department of Justice, Rating, Titles and Qualification",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("BR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeGame, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvApp},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "ER", minRecommendAge: 9, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 0},
		&baseRating{name: "L-18", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 1},
		&baseRating{name: "L-10", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 1},
		&baseRating{name: "L", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "L-12", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 1},
		&baseRating{name: "L-14", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 1},
		&baseRating{name: "L-16", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 1},
		&baseRating{name: "10-12", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 6},
		&baseRating{name: "10", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "10-18", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 6},
		&baseRating{name: "10-16", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 6},
		&baseRating{name: "10-14", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 6},
		&baseRating{name: "12-18", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 9},
		&baseRating{name: "12-16", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 9},
		&baseRating{name: "12-14", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 9},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "14-16", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 12},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "14-18", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 12},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "16-18", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 15},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
	},
}

var EE = &baseSystem{
	acronym: "EE",
	name:    "",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("EE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "Pere", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "L", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "MS-6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "K-6", minRecommendAge: 6, minAllowedAgeSupervised: 6, minAllowedAgeUnsupervised: 6, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "MS-12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "K-12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "K-14", minRecommendAge: 14, minAllowedAgeSupervised: 14, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 18},
		&baseRating{name: "K-16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 80},
	},
}

var EG = &baseSystem{
	acronym: "EG",
	name:    "",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("EG"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "GN", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "18", minRecommendAge: 0, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "BN", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var EIRIN = &baseSystem{
	acronym: "EIRIN",
	name:    "EIGA RINRI IINKAI",
	typ:     OrgOther,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("JP"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "R15+", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var ESRB = &baseSystem{
	acronym: "ESRB",
	name:    "Entertainment Software Rating Board",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("US"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "RP", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 0},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "10-plus", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 60},
		&baseRating{name: "T", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 70},
		&baseRating{name: "M", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "A", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
	},
}

var FAB = &baseSystem{
	acronym: "FAB",
	name:    "Film Advisory Board",
	typ:     OrgConsumer,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("US"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "F", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PD", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "PD-M", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "M", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 12},
		&baseRating{name: "VM", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 15},
		&baseRating{name: "EM", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "AO", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
	},
}

var FCBM = &baseSystem{
	acronym: "FCBM",
	name:    "Film Censorship Board of Malaysia",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("MY"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "U", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "P13", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "18SX", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: false, ordinal: 80},
		&baseRating{name: "18PA", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: false, ordinal: 80},
		&baseRating{name: "18SG", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: false, ordinal: 80},
		&baseRating{name: "18PL", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: false, ordinal: 80},
	},
}

var FCO = &baseSystem{
	acronym: "FCO",
	name:    "Office for Film, Newspaper and Article Administration",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("HK"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "I", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "IIA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "IIB", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "III", minRecommendAge: 0, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var FPB = &baseSystem{
	acronym: "FPB",
	name:    "Film & Publication Board",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("ZA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeGame, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "7-9-PG", minRecommendAge: 7, minAllowedAgeSupervised: 7, minAllowedAgeUnsupervised: 9, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "10", minRecommendAge: 10, minAllowedAgeSupervised: 10, minAllowedAgeUnsupervised: 10, hpcApplicable: true, ordinal: 10},
		&baseRating{name: "10M", minRecommendAge: 10, minAllowedAgeSupervised: 10, minAllowedAgeUnsupervised: 10, hpcApplicable: true, ordinal: 10},
		&baseRating{name: "10-12-PG", minRecommendAge: 10, minAllowedAgeSupervised: 10, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 11},
		&baseRating{name: "13", minRecommendAge: 13, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "X18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "XX", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var FSK = &baseSystem{
	acronym: "FSK",
	name:    "Freiwillige Selbstkontrolle der Filmwirtschaft",
	typ:     OrgOther,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("DE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "0", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "Keine", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var GR = &baseSystem{
	acronym: "GR",
	name:    "",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("GR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "K", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "K-13", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "K-17", minRecommendAge: 17, minAllowedAgeSupervised: 17, minAllowedAgeUnsupervised: 17, hpcApplicable: true, ordinal: 80},
	},
}

var IBMCT = &baseSystem{
	acronym: "IBMCT",
	name:    "Inspection Board, Ministry of Culture and Tourism",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("TR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "GA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7A", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 7, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 7, minAllowedAgeUnsupervised: 7, hpcApplicable: true, ordinal: 7},
		&baseRating{name: "13A", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "13", minRecommendAge: 13, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 13},
		&baseRating{name: "15A", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 14},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 18},
	},
}

var ICAA = &baseSystem{
	acronym: "ICAA",
	name:    "",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("ES"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "APTA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "ER", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "13", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 10},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "X", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var IFCO = &baseSystem{
	acronym: "IFCO",
	name:    "Irish Film Classification Office",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeDVD},
			Environments: []RestrictionEnv{EnvHome},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var IFCOF = &baseSystem{
	acronym: "IFCOF",
	name:    "Irish Film Classification Office",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 8, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12A", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "15A", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var IL = &baseSystem{
	acronym: "IL",
	name:    "Film Review Board",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "ALL", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14", minRecommendAge: 0, minAllowedAgeSupervised: 14, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 4},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var INCAA = &baseSystem{
	acronym: "INCAA",
	name:    "Comisión Asesora de Exhibiciones Cinematográficas, Instituto Nacional de Cine y Artes Audiovisuales",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("AR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "ATP", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "SAM13", minRecommendAge: 0, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 13},
		&baseRating{name: "SAM16", minRecommendAge: 0, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 16},
		&baseRating{name: "SAM18", minRecommendAge: 0, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 81},
	},
}

var KFCB = &baseSystem{
	acronym: "KFCB",
	name:    "Kenya Film Classification Board",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("KE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16+", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var Kijkwijzer = &baseSystem{
	acronym: "Kijkwijzer",
	name:    "Nederlands Instituut voor de Classificatie van Audiovisuele Media",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("NL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
		language.MustParseRegion("IS"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvHome},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "AL", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "9", minRecommendAge: 9, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
	},
}

var KMRB = &baseSystem{
	acronym: "KMRB",
	name:    "Korea Media Rating Board",
	typ:     OrgOther,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("KR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "12+", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "15+", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "LIM", minRecommendAge: 19, minAllowedAgeSupervised: 19, minAllowedAgeUnsupervised: 19, hpcApplicable: true, ordinal: 90},
	},
}

var LSF = &baseSystem{
	acronym: "LSF",
	name:    "Lembaga Sensor Film",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("ID"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeOther},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "SU", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "A", minRecommendAge: 3, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "13", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "17", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 17, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "21", minRecommendAge: 21, minAllowedAgeSupervised: 21, minAllowedAgeUnsupervised: 21, hpcApplicable: true, ordinal: 80},
	},
}

var MBACT = &baseSystem{
	acronym: "MBACT",
	name:    "Ministero dei Beni e delle Attività Culturali e del Turismo",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "T", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "VM14", minRecommendAge: 14, minAllowedAgeSupervised: 14, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "VM18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var MCCAA = &baseSystem{
	acronym: "MCCAA",
	name:    "Film Age-Classification Board",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("MT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "U", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12A", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: false, ordinal: 6},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 14, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: false, ordinal: 18},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var MCCYP = &baseSystem{
	acronym: "MCCYP",
	name:    "The Media Council for Children and Young People",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("DK"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 4, minAllowedAgeSupervised: 4, minAllowedAgeUnsupervised: 4, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "11", minRecommendAge: 11, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var MCST = &baseSystem{
	acronym: "MCST",
	name:    "Ministry of Culture, Sports and Tourism of Viet Nam",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("VN"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTV},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "0", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "P", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "C13", minRecommendAge: 13, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 13},
		&baseRating{name: "C16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 16},
		&baseRating{name: "16+", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 16},
		&baseRating{name: "C18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 18},
		&baseRating{name: "NYR", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var MDA = &baseSystem{
	acronym: "MDA",
	name:    "Infocomm Media Development Authority",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("SG"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeAd},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "PG13", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "NC16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "M18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "R21", minRecommendAge: 21, minAllowedAgeSupervised: 21, minAllowedAgeUnsupervised: 21, hpcApplicable: true, ordinal: 80},
	},
}

var Medietilsynet = &baseSystem{
	acronym: "Medietilsynet",
	name:    "Medietilsynet",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("NO"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "9", minRecommendAge: 9, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 4},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "11", minRecommendAge: 11, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 8},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var MEKU = &baseSystem{
	acronym: "MEKU",
	name:    "Media Education and Image Program Unit, National Audiovisual Institute",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("FI"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeGame, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "S", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 10, minAllowedAgeUnsupervised: 7, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 19, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var MFCB = &baseSystem{
	acronym: "MFCB",
	name:    "Maritime Film Classification Board",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14A", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18A", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 75},
		&baseRating{name: "R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "A", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
	},
}

var MKRF = &baseSystem{
	acronym: "MKRF",
	name:    "Ministry of Culture of the Russian Federation",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("RU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeMusic, TypeGame, TypeTV, TypeAd, TypeOther},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "0", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "6", minRecommendAge: 6, minAllowedAgeSupervised: 6, minAllowedAgeUnsupervised: 6, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var MOC = &baseSystem{
	acronym: "MOC",
	name:    "MoC",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CO"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "T", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "X", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "Banned", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var MOC_TW = &baseSystem{
	acronym: "MOC-TW",
	name:    "Ministry of Culture - Tawan",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("TW"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "P", minRecommendAge: 0, minAllowedAgeSupervised: 6, minAllowedAgeUnsupervised: 11, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R", minRecommendAge: 0, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var MPAA = &baseSystem{
	acronym: "MPAA",
	name:    "Motion Picture Association of America, Inc.",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("US"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "M", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 3},
		&baseRating{name: "GP", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 3},
		&baseRating{name: "PG-13", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "NC-17", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "X", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: false, ordinal: 80},
	},
}

var MPAAT = &baseSystem{
	acronym: "MPAAT",
	name:    "Motion Picture Association of America, Inc.",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("US"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTrailer, TypeDVD, TypeAd},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "GB", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "RB", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
	},
}

var MTRCB = &baseSystem{
	acronym: "MTRCB",
	name:    "MOVIE AND TELEVISION REVIEW AND CLASSIFICATION BOARD",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("PH"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV, TypeAd},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G-TV", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG-TV", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "PG", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "SPG", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R13", minRecommendAge: 13, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "R18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "X", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var NBC = &baseSystem{
	acronym: "NBC",
	name:    "National Bureau of Classification",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("MV"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12+", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "15+", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "18+R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "PU", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 95},
	},
}

var NBC_PL = &baseSystem{
	acronym: "NBC-PL",
	name:    "National Broadcasting Council",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("PL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "I", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "II", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "III", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "IV", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var NCS = &baseSystem{
	acronym: "NCS",
	name:    "Classification Board, Department of Communications and the Arts",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("AU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeGame},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail, EnvApp},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "M", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "MA15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "CTC", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "R18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "X18+", minRecommendAge: 0, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "RC", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var NFRC = &baseSystem{
	acronym: "NFRC",
	name:    "National Film Centre",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("BG"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "B", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "C", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "D", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "X", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var NFVCB = &baseSystem{
	acronym: "NFVCB",
	name:    "National Film and Video Censors' Board",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("NG"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 12, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "12A", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 12, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "RE", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: false, ordinal: 95},
	},
}

var NKC_LV = &baseSystem{
	acronym: "NKC-LV",
	name:    "National Film Center of Latvia",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("LV"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "U", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7+", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12+", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "16+", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 8},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var OFLC = &baseSystem{
	acronym: "OFLC",
	name:    "Office of FIlm and Literature Classification",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("NZ"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeDVD, TypeGame},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvOther},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "RP13", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "R13", minRecommendAge: 13, minAllowedAgeSupervised: 13, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "R15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "M", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 15},
		&baseRating{name: "RP16", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 18},
		&baseRating{name: "R16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 21},
		&baseRating{name: "RP18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 79},
		&baseRating{name: "R18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
		&baseRating{name: "R", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 95},
	},
}

var OFRB = &baseSystem{
	acronym: "OFRB",
	name:    "Ontario Film Review Board,",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14A", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18A", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 75},
		&baseRating{name: "R", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var PE = &baseSystem{
	acronym: "PE",
	name:    "",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("PE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "PT", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PG", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var PEGI = &baseSystem{
	acronym: "PEGI",
	name:    "Pan European Game Information",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("AT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("BE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("BG"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("CY"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("CZ"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("DK"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("EE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("FI"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("FR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("GR"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("HU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("IS"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("IE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("IL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("IT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("LV"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("LT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("LU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("MT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("NL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("NO"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("PL"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("PT"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("RO"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("SK"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("SI"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("ES"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("SE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("CH"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
		language.MustParseRegion("GB"): RegionalRestrictions{
			Types:        []RestrictionType{TypeGame},
			Environments: []RestrictionEnv{EnvHome},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "PEGI-OK", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PEGI-3", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PEGI-7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 7},
		&baseRating{name: "PEGI-12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "PEGI-16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 16},
		&baseRating{name: "PEGI-18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var RCNOF = &baseSystem{
	acronym: "RCNOF",
	name:    "NATIONAL MEDIA AND COMMUNICATIONS AUTHORITY",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("HU"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTV},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "I", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "II", minRecommendAge: 6, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "III", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "IV", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "V", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "VI", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var RCQ = &baseSystem{
	acronym: "RCQ",
	name:    "Ministre de la Culture et des Communications",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("CA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "13+", minRecommendAge: 13, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 13, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "16+", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "18+", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "RC", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var RESORTE_Health = &baseSystem{
	acronym: "RESORTE-Health",
	name:    "SiBCI",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("VE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeMusic, TypeTV, TypeAd},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "B", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "D", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var RESORTE_Language = &baseSystem{
	acronym: "RESORTE-Language",
	name:    "SiBCI",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("VE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeMusic, TypeTV, TypeAd},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "B", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var RESORTE_Sexo = &baseSystem{
	acronym: "RESORTE-Sexo",
	name:    "SiBCI",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("VE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeMusic, TypeTV, TypeAd},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "B", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "D", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var RESORTE_Violencia = &baseSystem{
	acronym: "RESORTE-Violencia",
	name:    "SiBCI",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("VE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeMusic, TypeTV, TypeAd},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "B", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "C", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "D", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "E", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var RIAA = &baseSystem{
	acronym: "RIAA",
	name:    "Recording Industry Association of America",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("US"): RegionalRestrictions{
			Types:        []RestrictionType{TypeMusic},
			Environments: []RestrictionEnv{EnvHome, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "PAL", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var RTC = &baseSystem{
	acronym: "RTC",
	name:    "DIRECCIÓN GENERAL DE RADIO, TELEVISIÓN Y CINEMATOGRAFÍA",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("MX"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "AA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "A", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "B", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "B15", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "D", minRecommendAge: 19, minAllowedAgeSupervised: 19, minAllowedAgeUnsupervised: 19, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "C", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 80},
	},
}

var RTE = &baseSystem{
	acronym: "RTE",
	name:    "Raidió Teilifís Éireann",
	typ:     OrgOther,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "GA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "Ch", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "PS", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "MA", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var SK = &baseSystem{
	acronym: "SK",
	name:    "",
	typ:     OrgNotSpecified,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("SK"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "P2", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "P5", minRecommendAge: 15, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "P8", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var SM_SA = &baseSystem{
	acronym: "SM-SA",
	name:    "Statens medieråd (National Media Council)",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("SE"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer},
			Environments: []RestrictionEnv{EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "Barntillåten", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 7, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "11", minRecommendAge: 11, minAllowedAgeSupervised: 7, minAllowedAgeUnsupervised: 11, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "15", minRecommendAge: 15, minAllowedAgeSupervised: 15, minAllowedAgeUnsupervised: 15, hpcApplicable: true, ordinal: 80},
	},
}

var SMAIS = &baseSystem{
	acronym: "SMAIS",
	name:    "SMAIS",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("IS"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeGame},
			Environments: []RestrictionEnv{EnvHome, EnvTheater},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "L", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "12", minRecommendAge: 12, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 9},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 12},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 15},
	},
}

var TVPG = &baseSystem{
	acronym: "TVPG",
	name:    "TV Parental Guidelines Monitoring Board",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("US"): RegionalRestrictions{
			Types:        []RestrictionType{TypeTV},
			Environments: []RestrictionEnv{EnvHome, EnvBroadcast},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "TV-Y", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "TV-G", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "TV-Y7", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 6},
		&baseRating{name: "TV-Y7-FV", minRecommendAge: 7, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 7},
		&baseRating{name: "TV-PG", minRecommendAge: 10, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 10},
		&baseRating{name: "TV-14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 14},
		&baseRating{name: "TV-MA", minRecommendAge: 17, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 80},
	},
}

var Ukraine = &baseSystem{
	acronym: "Ukraine",
	name:    "Ukrainian State Film Agency",
	typ:     OrgGovernment,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("UA"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeTV},
			Environments: []RestrictionEnv{EnvTheater, EnvBroadcast, EnvRetail},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "Yes", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "For", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "14", minRecommendAge: 14, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 14, hpcApplicable: true, ordinal: 1},
		&baseRating{name: "16", minRecommendAge: 16, minAllowedAgeSupervised: 16, minAllowedAgeUnsupervised: 16, hpcApplicable: true, ordinal: 2},
		&baseRating{name: "18", minRecommendAge: 18, minAllowedAgeSupervised: 18, minAllowedAgeUnsupervised: 18, hpcApplicable: true, ordinal: 3},
		&baseRating{name: "X21", minRecommendAge: 21, minAllowedAgeSupervised: 21, minAllowedAgeUnsupervised: 21, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "Denied", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 100},
	},
}

var UNRATED = &baseSystem{
	acronym: "UNRATED",
	name:    "Motion Picture Laboratories, Inc.",
	typ:     OrgTrade,
	regions: map[language.Region]RegionalRestrictions{
		language.MustParseRegion("ZZ"): RegionalRestrictions{
			Types:        []RestrictionType{TypeFilm, TypeTrailer, TypeDVD, TypeMusic, TypeGame, TypeTV, TypeAd, TypeOther},
			Environments: []RestrictionEnv{EnvHome, EnvTheater, EnvBroadcast, EnvRetail, EnvApp, EnvOther},
		},
	},
	ratings: []*baseRating{
		&baseRating{name: "ALL", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 0},
		&baseRating{name: "UNRATED", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 80},
		&baseRating{name: "ADULT", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: true, ordinal: 90},
		&baseRating{name: "PROSCRIBED", minRecommendAge: 0, minAllowedAgeSupervised: 0, minAllowedAgeUnsupervised: 0, hpcApplicable: false, ordinal: 100},
	},
}
