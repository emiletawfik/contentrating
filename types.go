// Package contentrating provides functions and types to use parse and use content rating
package contentrating

import (
	"fmt"

	"golang.org/x/text/language"
)

// Source https://movielabs.com/md/ratings/v2.3.2/html/Summary.html

// Represent a type of restriction
type RestrictionType uint8

// Arbitrary non exhaustive list of restriction type
const (
	TypeOther   RestrictionType = iota + 1 // 1
	TypeFilm                               // 2
	TypeTrailer                            // 3
	TypeDVD                                // 4
	TypeGame                               // 5
	TypeTV                                 // 6
	TypeMusic                              // 7
	TypeAd                                 // 8
)

func (rt RestrictionType) String() string {
	switch rt {
	case TypeOther:
		return "Other"
	case TypeFilm:
		return "Film"
	case TypeTrailer:
		return "Trailer"
	case TypeDVD:
		return "DVD"
	case TypeGame:
		return "Game"
	case TypeTV:
		return "TV"
	case TypeMusic:
		return "Music"
	case TypeAd:
		return "Ad"
	default:
		return ""
	}
}

// Represent an environnement of restriction
type RestrictionEnv uint8

// Arbitrary non exhaustive list of restriction type
const (
	EnvOther     RestrictionEnv = iota + 1 // 1
	EnvTheater                             // 2
	EnvBroadcast                           // 3
	EnvHome                                // 4
	EnvRetail                              // 5
	EnvApp                                 // 6
)

func (re RestrictionEnv) String() string {
	switch re {
	case EnvOther:
		return "Other"
	case EnvTheater:
		return "Theater"
	case EnvBroadcast:
		return "Broadcast"
	case EnvHome:
		return "Home"
	case EnvRetail:
		return "Retail"
	case EnvApp:
		return "App"
	default:
		return ""
	}
}

// Represent an organisation type
type OrgType uint8

// Arbitrary non exhaustive list of organisation type
const (
	OrgGovernment   OrgType = iota + 1 // 1
	OrgTrade                           // 2
	OrgNotSpecified                    // 3
	OrgConsumer                        // 4
	OrgOther                           // 5
)

func (ot OrgType) String() string {
	switch ot {
	case OrgGovernment:
		return "Government"
	case OrgTrade:
		return "Trade"
	case OrgNotSpecified:
		return "Not Specified"
	case OrgConsumer:
		return "Consumer"
	case OrgOther:
		return "Other"
	default:
		return ""
	}
}

type RegionalRestrictions struct {
	Types        []RestrictionType
	Environments []RestrictionEnv
}

type System interface {
	fmt.Stringer
	Acronym() string
	OrgName() string
	OrgType() OrgType
	Regions() []language.Region
	RegionalRestrictions() map[language.Region]RegionalRestrictions
	Ratings() []Rating
}

type Rating interface {
	fmt.Stringer
	System() System
	Name() string
	MinRecommendAge() int
	MinAllowedAgeSupervised() int
	MinAllowedAgeUnsupervised() int
	HPCApplicable() bool
	Ordinal() int
}

type baseSystem struct {
	acronym string
	name    string
	typ     OrgType
	regions map[language.Region]RegionalRestrictions
	ratings []*baseRating
}

func (s *baseSystem) Acronym() string {
	return s.acronym
}

func (s *baseSystem) OrgName() string {
	return s.name
}

func (s *baseSystem) OrgType() OrgType {
	return s.typ
}

func (s *baseSystem) Regions() []language.Region {
	ret := make([]language.Region, 0, len(s.regions))
	for r := range s.regions {
		ret = append(ret, r)
	}
	return ret
}

func (s *baseSystem) RegionalRestrictions() map[language.Region]RegionalRestrictions {
	return s.regions
}

func (s *baseSystem) Ratings() []Rating {
	ret := make([]Rating, 0, len(s.ratings))
	for _, r := range s.ratings {
		ret = append(ret, r)
	}
	return ret
}

func (s *baseSystem) String() string {
	return s.name
}

type baseRating struct {
	system                    *baseSystem
	name                      string
	minRecommendAge           int
	minAllowedAgeSupervised   int
	minAllowedAgeUnsupervised int
	hpcApplicable             bool
	ordinal                   int
}

func (r *baseRating) System() System {
	return r.system
}

func (r *baseRating) MinRecommendAge() int {
	return r.minRecommendAge
}

func (r *baseRating) MinAllowedAgeSupervised() int {
	return r.minAllowedAgeSupervised
}

func (r *baseRating) MinAllowedAgeUnsupervised() int {
	return r.minAllowedAgeUnsupervised
}

func (r *baseRating) HPCApplicable() bool {
	return r.hpcApplicable
}

func (r *baseRating) Ordinal() int {
	return r.ordinal
}

func (r *baseRating) Name() string {
	return r.name
}

func (r *baseRating) String() string {
	return fmt.Sprintf("%s->%s", r.system.acronym, r.name)
}
